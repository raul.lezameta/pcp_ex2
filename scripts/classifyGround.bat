cd %~dp0

call variables.bat

opalsRobFilter -inFile %ODM_FEB_f% -filter echo[last]

@REM remove points with low amplitudes from ODM (unless they are below water Surface)
opalsAddInfo -inFile %ODM_OCT_f% -attribute "_dropAmplitude = 0"
opalsAddInfo -inFile %ODM_OCT_f% -attribute "_dropAmplitude = Amplitude < 1300 ? 1 : 0" -filter "Generic[_below_wSurf == 0]"
opalsExport -inFile %ODM_OCT_f% -outFile %ODM_OCT_fA% -filter "Generic[_dropAmplitude == 0]"
opalsRobFilter -inFile %ODM_OCT_fA% -filter echo[last]

opalsExport -inFile %ODM_FEB_f% -outFile %ODM_FEB_ground% -filter class[ground]
@REM opalsExport -inFile %ODM_OCT_fA% -outFile %ODM_OCT_ground% -filter class[ground]


@REM opalsAddInfo -inFile %ODM_OCT_f% -attribute "Classification = Amplitude > 1800 ? 2 : 0" -filter "Generic[_below_wSurf == 0]"
@REM opalsExport -inFile %ODM_OCT_f% -outFile %ODM_OCT_ground+% -filter "Generic[Classification == 2]"