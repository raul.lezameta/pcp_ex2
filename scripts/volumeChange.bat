cd %~dp0

call variables.bat

opalsHisto -inFile %DoD_cut% -plotFile %resPath%\hist.svg

opalsHisto -inFile %DoD_r% -plotFile %resPath%\hist_r.svg
opalsHisto -inFile %DoD_r+% -plotFile %resPath%\hist_r+.svg
opalsHisto -inFile %DoD_a% -plotFile %resPath%\hist_a.svg

opalsHisto -inFile %DoD_r2a% -plotFile %resPath%\hist_a2r.svg
opalsHisto -inFile %DoD_a2r% -plotFile %resPath%\hist_r2a.svg