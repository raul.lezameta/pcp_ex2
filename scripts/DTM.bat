cd %~dp0

call variables.bat

@REM use only points classified as ground to generate DTM
opalsDTM -inFile %ODM_FEB_f% -outFile %DTM_FEB_dtm% -filter "Generic[Classification == 2]" -feature
opalsDTM -inFile %ODM_OCT_fA% -outFile %DTM_OCT_dtm% -filter "Generic[Classification == 2]" -feature


@REM fill gaps in DTM;
opalsFillGaps -inFile %DTM_FEB_dtm%_dtm.tif -outFile %DTM_FEB_dtm_fG% -method adaptive -maxArea 2 
opalsFillGaps -inFile %DTM_OCT_dtm%_dtm.tif -outFile %DTM_OCT_dtm_fG% -method adaptive -maxArea 2

del DTM_FEB_dtm_dtm.tif
del DTM_FEB_dtm_dtm.vrt
del DTM_FEB_dtm_nx.tif
del DTM_FEB_dtm_ny.tif
del DTM_FEB_dtm_prec.tif

del DTM_OCT_dtm_dtm.tif
del DTM_OCT_dtm_dtm.vrt
del DTM_OCT_dtm_nx.tif
del DTM_OCT_dtm_ny.tif
del DTM_OCT_dtm_prec.tif