cd %~dp0

call variables.bat

@REM classify areas where dtm below wSurface as riverbed (1) and other areas as alluvial (0)

set classFormula="if r[0] == None: \n return 0 \nelif  r[0] < r[1]: \n return 1 \nelse: \n return 2"
opalsAlgebra -inFile %DTM_FEB_corr_dtm_fG% %WSURF_FEB% -outFile %class_FEB% -formula %classFormula%
opalsAlgebra -inFile %DTM_OCT_dtm_fG% %WSURF_OCT% -outFile %class_OCT% -formula %classFormula%
