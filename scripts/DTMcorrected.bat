cd %~dp0

call variables.bat



@REM set DTM_FEB_robF=%procPath%\DTM_FEB_robF.tif
@REM opalsGrid -inFile %ODM_FEB_f% -outFile %DTM_FEB_robF% -interpolation movingPlanes -filter class[ground]


@REM set DTM_FEB_corr_dtm=%procPath%\DTM_FEB_corr_dtm
@REM set DTM_FEB_corr_dtm_fG=%procPath%\DTM_FEB_corr_dtm_fG.tif

@REM use only points classified as ground to generate DTM + reclassified points
opalsDTM -inFile %ODM_FEB_fc% -outFile %DTM_FEB_corr_dtm% -filter "Generic[Classification == 2]" -feature
@REM opalsDTM -inFile %ODM_OCT_fc% -outFile %DTM_OCT_corr_dtm% -filter "Generic[Classification == 2]"


@REM fill gaps in DTM
opalsFillGaps -inFile %DTM_FEB_corr_dtm%_dtm.tif -outFile %DTM_FEB_corr_dtm_fG% -method adaptive -maxArea 2
@REM opalsFillGaps -inFile %DTM_OCT_corr_dtm%_dtm.tif -outFile %DTM_OCT_corr_dtm_fG% -method adaptive -maxArea 2


del DTM_FEB_corr_dtm_dtm.tif
del DTM_FEB_corr_dtm_dtm.vrt
del DTM_FEB_corr_dtm_nx.tif
del DTM_FEB_corr_dtm_ny.tif
del DTM_FEB_corr_dtm_prec.tif

del DTM_OCT_corr_dtm_dtm.tif
del DTM_OCT_corr_dtm_dtm.vrt
del DTM_OCT_corr_dtm_nx.tif
del DTM_OCT_corr_dtm_ny.tif
del DTM_OCT_corr_dtm_prec.tif