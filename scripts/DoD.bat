cd %~dp0

call variables.bat

opalsAlgebra -inFile %DTM_FEB_corr_dtm_fG% %DTM_OCT_dtm_fG% -outFile %DoD% -formula "r[1] - r[0]"
opalsAlgebra -inFile %DoD% -outFile %DoD_cut% -formula "if abs(r[0]) > 1.5: \n return None \nelse: \n return r[0]"

@REM calculate DoD riverbed/alluvial (areas classfied as riverbed/alluvial in FEB AND OCT); (r = 1; a = 2)
opalsAlgebra -inFile %DoD_cut% %class_FEB% %class_OCT% -outFile %DoD_r% -formula "if ((r[1] == 1) and (r[2] == 1)): \n return r[0] \nelse: \n return None"
opalsAlgebra -inFile %DoD_cut% %class_FEB% %class_OCT% -outFile %DoD_a% -formula "if ((r[1] == 2) and (r[2] == 2)): \n return r[0] \nelse: \n return None"

@REM calculate DoD riverbed+ (areas classified as riverbed either in FEB or OCT);  (r = 1; a = 2)
opalsAlgebra -inFile %DoD_cut% %class_FEB% %class_OCT% -outFile %DoD_r+% -formula "if ((r[1] == 1) or (r[2] == 1)): \n return r[0] \nelse: \n return None"


@REM calculate DoD riverbed2alluvial; alluvial2riverbed; (r = 1; a = 2)
opalsAlgebra -inFile %DoD_cut% %class_FEB% %class_OCT% -outFile %DoD_r2a% -formula "if ((r[1] == 1) and (r[2] == 2)): \n return r[0] \nelse: \n return None"
opalsAlgebra -inFile %DoD_cut% %class_FEB% %class_OCT% -outFile %DoD_a2r% -formula "if ((r[1] == 2) and (r[2] == 1)): \n return r[0] \nelse: \n return None"