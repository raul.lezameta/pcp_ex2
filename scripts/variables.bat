cd %~dp0

set inPath=..\data\input
set procPath=..\data\processing
set resPath=..\data\results

mkdir %procPath%
mkdir %resPath%

mkdir %procPath%\ODM
mkdir %procPath%\FEB
mkdir %procPath%\OCT

set ALS_FEB=%inPath%\2014_FEB.laz
set ALS_OCT=%inPath%\2014_OCT.laz

set ODM_FEB=%procPath%\ODM\2014_FEB.odm
set ODM_OCT=%procPath%\ODM\2014_OCT.odm

set WSURF_FEB=%inPath%\PROFIL-2014-FEB.tif
set WSURF_OCT=%inPath%\PROFIL-2014-OCT.tif


set ODM_FEB_f=%procPath%\ODM\2014_FEB_filtered.odm
set ODM_OCT_f=%procPath%\ODM\2014_OCT_filtered.odm
set ODM_OCT_fA=%procPath%\ODM\2014_OCT_filteredA.odm

set ODM_FEB_fc=%procPath%\ODM\2014_FEB_filtered_corrected.odm
set ODM_OCT_fc=%procPath%\ODM\2014_OCT_filtered_corrected.odm

set ODM_FEB_ground=%procPath%\ODM\2014_FEB_ground.odm
set ODM_OCT_ground=%procPath%\ODM\2014_OCT_ground.odm
set ODM_OCT_ground+=%procPath%\ODM\2014_OCT_ground+.odm



set DTM_FEB_dtm=%procPath%\FEB\DTM_FEB_dtm
set DTM_OCT_dtm=%procPath%\OCT\DTM_OCT_dtm
set DTM_FEB_dtm_fG=%resPath%\DTM_FEB_dtm_fG.tif
set DTM_OCT_dtm_fG=%resPath%\DTM_OCT_dtm_fG.tif


set DTM_FEB_corr_dtm=%procPath%\FEB\DTM_FEB_corr_dtm
set DTM_OCT_corr_dtm=%procPath%\OCT\DTM_OCT_corr_dtm
set DTM_FEB_corr_dtm_fG=%resPath%\DTM_FEB_corr_dtm_fG.tif
set DTM_OCT_corr_dtm_fG=%resPath%\DTM_OCT_corr_dtm_fG.tif

set class_FEB=%resPath%\class_FEB.tif
set class_OCT=%resPath%\class_OCT.tif

set DoD=%resPath%\DoD.tif
set DoD_cut=%resPath%\DoD_cut.tif

set DoD_r=%resPath%\DoD_riverbed.tif
set DoD_r+=%resPath%\DoD_riverbed+.tif
set DoD_a=%resPath%\DoD_alluvial.tif

set DoD_r2a=%resPath%\DoD_riverbed2alluvial.tif
set DoD_a2r=%resPath%\DoD_alluvial2riverbed.tif