@echo off

cd %~dp0

call variables.bat

call importData.bat

call flyingPoints.bat

call classifyGround.bat

call DTM.bat

@REM reclassify points in area where DTM could not be generated as Ground points (only points with high amplitude) -  only for FEB
call reclassifyGaps.bat

@REM recalculate DTM with newly classified ground points
call DTMcorrected.bat

@REM classify areas where dtm below wSurface as riverbed (1) and other areas as alluvial (0)
call classifyAlluvialRiverbed.bat

@REM calculate DoD
call DoD.bat

@REM histograms to visualize volume change
call volumeChange.bat