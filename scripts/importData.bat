cd %~dp0

call variables.bat

opalsImport -inFile %ALS_FEB% -outFile %ODM_FEB%
opalsImport -inFile %ALS_OCT% -outFile %ODM_OCT%

@REM create new Attribute (1 if pnts are below the water surface)
opalsAddInfo -inFile %ODM_FEB% -gridFile %WSURF_FEB% -attribute "_below_wSurf = z < r[0] ? 1 : 0"
opalsAddInfo -inFile %ODM_OCT% -gridFile %WSURF_OCT% -attribute "_below_wSurf = z < r[0] ? 1 : 0"
