cd %~dp0

call variables.bat

@REM classify flying points as 2 else Classification (0)
opalsAddInfo -inFile %ODM_FEB% -searchRadius 3 -searchMode d3 -neighbours 5 -attribute "Classification = count(n) <= 3 ? 2 : Classification"
opalsAddInfo -inFile %ODM_OCT% -searchRadius 3 -searchMode d3 -neighbours 5 -attribute "Classification = count(n) <= 3 ? 2 : Classification"


@REM remove flying points and export new ODM
opalsExport -inFile %ODM_FEB% -outFile %ODM_FEB_f% -filter "Generic[Classification!=2]"
opalsExport -inFile %ODM_OCT% -outFile %ODM_OCT_f% -filter "Generic[Classification!=2]"
