cd %~dp0

call variables.bat

copy %ODM_FEB_f% %ODM_FEB_fc%
@REM copy %ODM_OCT_f% %ODM_OCT_fc%

opalsAddInfo -inFile %ODM_FEB_fc% -gridFile %DTM_FEB_dtm_fG% -attribute "Classification = r[0] ? Classification : 2" -filter "Generic[Amplitude > 800]"
@REM opalsAddInfo -inFile %ODM_OCT_fc% -gridFile %DTM_OCT_dtm_fG% -attribute "Classification = r[0] ? Classification : 2" -filter "Generic[Amplitude > 800]"

opalsAddInfo -inFile %ODM_FEB_fc% -gridFile %DTM_FEB_dtm_fG% -attribute "_changed = r[0] ? 0 : 1" -filter "Generic[Amplitude > 800]"
@REM opalsAddInfo -inFile %ODM_OCT_fc% -gridFile %DTM_OCT_dtm_fG% -attribute "_changed = r[0] ? 0 : 1" -filter "Generic[Amplitude > 800]"